import os
import sys
import types
import numpy
import keras
import pickle
import pandas
import marshal
import seaborn
import autofeat
import warnings
import matplotlib
import tensorflow
import matplotlib.pyplot
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from autofeat import FeatureSelector, AutoFeatRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_file = pickle.load(open(f"{PICKLE_PATH}/train_file.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    create_training_and_test_data = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    normalization = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/normalization.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_extraction = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_extraction.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    data_cleaning = types.FunctionType(marshal.loads(pickle.load(open(f"{PICKLE_PATH}/data_cleaning.pkl", "rb"))), {})
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = data_transformation
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_file"]
## $xpr_param_global_methods = ["create_training_and_test_data", "normalization","data_extraction","data_cleaning"]



warnings.filterwarnings("ignore")
def create_training_and_test_data(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)
    return X_train, X_test, y_train, y_test
def normalization(X_train, X_test):
    scaler = QuantileTransformer(n_quantiles=10, output_distribution='normal')
    X_train_normalized = scaler.fit_transform(X_train)
    X_test_normalized = scaler.transform(X_test)
    return X_train_normalized, X_test_normalized

try:
    pickle.dump(marshal.dumps(create_training_and_test_data.__code__), open(f"{PICKLE_PATH}/create_training_and_test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(normalization.__code__), open(f"{PICKLE_PATH}/normalization.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_extraction.__code__), open(f"{PICKLE_PATH}/data_extraction.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(marshal.dumps(data_cleaning.__code__), open(f"{PICKLE_PATH}/data_cleaning.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))


try:
    pickle.dump(train_file, open(f"{PICKLE_PATH}/train_file.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

